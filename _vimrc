set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" 设置其他依赖库文件位置
let $PATH = $PATH . ";" . $VIM . "/OtherLibs"

" 设置Python路径
let $PATH = $PATH . ";" . "D:\\Python27"
"let $PATH = $PATH . ";" . "D:\\Python27\\Lib"
"let $PATH = $PATH . ";" . "D:\\Python27\\DLLs"

" 设置git 路径, 以便vundle 调用
let $PATH = $PATH . ";" . "D:\\PortableGit\\bin"
let $PATH = $PATH . ";" . "D:\\PortableGit\\cmd"

" 绿色版Python需要设置DLLs来支持python-mode
let $PYTHONPATH = "D:\\Python27"
let $PYTHONPATH = $PYTHONPATH . ";" . "D:\\Python27\\Lib"
let $PYTHONPATH = $PYTHONPATH . ";" . "D:\\Python27\\DLLs"
"let $PYTHONPATH = $PYTHONPATH . ";" . $VIM . "\\..\\..\\..\\MovablePython\\movpy\\lib\\library.zip"


"Vundle setting

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=$VIM/vimfiles/bundle/Vundle.vim/
"set rtp+=~/.vim/bundle/Vundle.vim
" let vpath = '$VIM/vimfiles/bundle'
call vundle#begin('$VIM/vimfiles/bundle')
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'klen/Python-mode'
Plugin 'davidhalter/jedi-vim'
Plugin 'joonty/vdebug'
" Plugin 'rkulla/pydiction'
Plugin 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plugin 'honza/vim-snippets'

Plugin 'majutsushi/tagbar'
Plugin 'ervandew/supertab'
Plugin 'scrooloose/syntastic'
Plugin 'Shougo/unite.vim'
Plugin 'Shougo/vimproc.vim'
Plugin 'Shougo/neomru.vim'
Plugin 'Shougo/vimfiler.vim'
Plugin 'Shougo/neoyank.vim'
Plugin 'tpope/vim-dispatch'
Plugin 'OmniSharp/omnisharp-vim'


" for line alignment
" Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'tpope/vim-surround'
Plugin 'easymotion/vim-easymotion'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
"Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"Plugin 'L9'
" Git plugin not hosted on GitHub
"Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"Plugin 'rstacruz/sparkup', {'rtp': 'vim'}
" Avoid a name conflict with L9
"Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


" 设置编码自动识别, 中文引号显示
"let &termencoding=&encoding
set encoding=utf-8  "内部编码
set fileencoding=utf-8  "编辑的文件的编码
set fileencodings=ucs-bom,utf-8,chinese,latin1  "检测编码列表
"set helplang=cn " 帮助文件语言为中文
"set langmenu=zh_CN.UTF-8 " 菜单语言选项

"防止菜单乱码
if(has("win32") || has("win95") || has("win64") || has("win16"))
    source $VIMRUNTIME/delmenu.vim
    source $VIMRUNTIME/menu.vim
    language messages zh_CN.utf-8
endif
"双字节显示特殊字符
if v:lang =~? '^\(zh\)\|\(ja\)\|\(ko\)'
    set ambiwidth=double
endif
"不自动设置字节序标记
set nobomb


"设置主题ColorScheme
colorscheme molokai

" 设置字体
if has("gui_running")
    set guifont=YaHei\ Consolas\ Hybrid:h10
"    set guifont=Source\ Code\ Pro:h12
endif

" 允许退格键删除和tab操作
set smartindent
set smarttab
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set backspace=2
set textwidth=79

set nu " 启用行号

" 设置高宽
if has("gui_running")
  " GUI is running or is about to start.
  " Maximize gvim window.
  set lines=50 columns=200
else
  " This is console Vim.
  if exists("+lines")
    set lines=50
  endif
  if exists("+columns")
    set columns=100
  endif
endif


" 强制将.md文件设置为markdown
autocmd BufNewFile,BufReadPost *.mdown,*.mkd,*.mkdn,*.md set filetype=markdown
" 针对 Web 开发，统一缩进2个空格
autocmd BufNewFile,BufRead *.html,*.htm,*.css,*.js set tabstop=2 softtabstop=2 shiftwidth=2


" 开启关闭自动备份
"set nobackup
"set backup
" 设置自动备份目录
set backupdir=$Vim/backup
" 设置自动备份文件名后缀
set backupext=.bak
" 保存原始版本
"set patchmode=.orig



"Pathogen setting
"execute pathogen#infect('D:/Vim/vimfiles/pathogen/bundle/{}')
""execute pathogen#helptags()
"syntax on
"filetype plugin indent on



"Python Mode Setting
"let g:pymode = 1
let g:pymode_rope = 0  "close rope, avoid auto generate .ropeproject in not python path

"let g:pymode_paths = ['D:\\Python27\\','D:\\Python27\\Lib','D:\\Python27\\DLLs']
"let g:pymode_paths = ['D:\Python27\Lib\site-packages\PyQt4\']
"let g:pymode_virtualenv = 1
"let g:pymode_virtualenv_path = 'D:\MyENV\ENV'

" Jedi Settings
let g:jedi#completions_command = "<M-.>"
"避免与python mode 运行冲突
let g:jedi#rename_command = "<leader><leader>r"

"Pydiction Settings
" let g:pydiction_location = 'D:\Vim\vimfiles\bundle\pydiction\complete-dict'

" UltiSnips Settings
"set nocompatible
"let g:UltiSnipsUsePythonVersion = 2

let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"



" Tagbar settings
"g:tagbar_ctags_bin='D:\Vim\vim74\ctags.exe'
"



" ---------------------------------------------
"Super tab settings - uncomment the next 4 lines
let g:SuperTabDefaultCompletionType = 'context'
"let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
"let g:SuperTabDefaultCompletionTypeDiscovery = ["&omnifunc:<c-x><c-o>","&completefunc:<c-x><c-n>"]
"let g:SuperTabClosePreviewOnPopupClose = 1



" EasyMotion Settings
let g:EasyMotion_do_mapping = 1 " set 0 to Disable default mappings 

" Bi-directional find motion
" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
"nmap s <Plug>(easymotion-s)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
"nmap s <Plug>(easymotion-s2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
"map <Leader>j <Plug>(easymotion-j)
"map <Leader>k <Plug>(easymotion-k)



" neoyank
let g:neoyank#file = $VIM . '/UniteCache/neoyank'

" neomru
let g:neomru#file_mru_path = $VIM . '/UniteCache/neomru/file'
let g:neomru#directory_mru_path = $VIM . '/UniteCache/neomru/directory'


" VimFiler
" Disable netrw.vim
" let g:loaded_netrwPlugin = 1
" vimfiler will be used as the default explorer (instead of netrw.)
let g:vimfiler_as_default_explorer = 1
let g:vimfiler_data_directory= $VIM . '/UniteCache/vimfiler'


" Unite
let g:unite_data_directory = $VIM . '/UniteCache/unite'
call unite#filters#matcher_default#use(['matcher_fuzzy'])

"let g:unite_ignore_source_files = ['*.png', '*.jpg', '*.tif', '*.meta']

" 所有ignorecase都无效
"call unite#custom#profile('ignorecase', 'context.ignorecase', 1)
"call unite#custom#profile('ignorecase', 'context.smartcase', 1)
"call unite#custom#profile('files', 'context.ignorecase', 1)
"call unite#custom#profile('files', 'context.smartcase', 1)
"call unite#custom#profile('buffer', 'context.ignorecase', 1)
"
"
" 不推荐在windows使用,太延迟
" nnoremap <M-u>t :<C-u>Unite -no-split -buffer-name=files -profile-name=ignorecase -start-insert file_rec/async:!<cr>
" let g:unite_source_rec_async_command = ['ag', '--follow', '--nocolor', '--nogroup', '--hidden', '-g', '']

"call unite#custom#source('file', 'ignore_globs', split(&wildignore, ','))
nnoremap <M-u>f :<C-u>Unite -no-split -buffer-name=files -profile-name=ignorecase -start-insert file<cr>

"call unite#custom#source('file_rec', 'ignore_globs', split(&wildignore, ','))
let g:unite_source_rec_find_args = ['-path', '*/.git/*', '-prune', '-o', '-type', 'l', '-print']
nnoremap <M-u>r :<C-u>Unite -no-split -buffer-name=files -profile-name=ignorecase -start-insert file_rec<cr>

nnoremap <M-u>h :<C-u>Unite -no-split -buffer-name=mru -profile-name=ignorecase -start-insert file_mru<cr>

" need outline plugin
"nnoremap <M-u>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>

let g:unite_source_history_yank_enable = 1
nnoremap <M-u>y :<C-u>Unite -no-split -buffer-name=yank -start-insert -auto-resize history/yank<cr>

nnoremap <M-u>b :<C-u>Unite -no-split -buffer-name=buffer -profile-name=buffer buffer<cr>

let g:unite_source_grep_max_candidates = 200

	if executable('hw')
	  " Use hw (highway)
	  " https://github.com/tkengo/highway
	  let g:unite_source_grep_command = 'hw'
	  let g:unite_source_grep_default_opts = '--no-group --no-color'
	  let g:unite_source_grep_recursive_opt = ''
	elseif executable('ag')
	  " Use ag (the silver searcher)
	  " https://github.com/ggreer/the_silver_searcher
	  let g:unite_source_grep_command = 'ag'
	  let g:unite_source_grep_default_opts =
	  \ '-i --vimgrep --hidden --ignore ' .
	  \ '''.hg'' --ignore ''.svn'' --ignore ''.git'' --ignore ''.bzr'''
	  let g:unite_source_grep_recursive_opt = ''
	elseif executable('pt')
	  " Use pt (the platinum searcher)
	  " https://github.com/monochromegane/the_platinum_searcher
	  let g:unite_source_grep_command = 'pt'
	  let g:unite_source_grep_default_opts = '--nogroup --nocolor'
	  let g:unite_source_grep_recursive_opt = ''
	elseif executable('ack-grep')
	  " Use ack
	  " http://beyondgrep.com/
	  let g:unite_source_grep_command = 'ack-grep'
	  let g:unite_source_grep_default_opts =
	  \ '-i --no-heading --no-color -k -H'
	  let g:unite_source_grep_recursive_opt = ''
	elseif executable('jvgrep')
	  " Use jvgrep
	  " https://github.com/mattn/jvgrep
	  let g:unite_source_grep_command = 'jvgrep'
	  let g:unite_source_grep_default_opts =
	  \ '-i --exclude ''\.(git|svn|hg|bzr)'''
	  let g:unite_source_grep_recursive_opt = '-R'
	endif

call unite#custom#source('grep', 'matchers', 'matcher_fuzzy')
nnoremap <M-u>g :<C-u>Unite -no-split -buffer-name=search -profile-name=ignorecase grep:.<cr>

"nnoremap <F9> :Unite -auto-resize -buffer-name=Unite_Menu menu<CR>
"inoremap <F9> :Unite -auto-resize -buffer-name=Unite_Menu menu<CR>
"
"nnoremap <C-L> :Unite -no-split -auto-resize -buffer-name=Lines line<CR>
"inoremap <C-L> :Unite -no-split -auto-resize -buffer-name=Lines line<CR>
"
"let g:unite_source_menu_menus = {}
"
"let g:unite_source_menu_menus.Unite = {
"    \ 'description' : ' > Unite 常用操作（文件、缓冲区）
"        \                            ',
"    \}
"
"let g:unite_source_menu_menus.Unite.command_candidates = [
"    \[' > 查看历史文件         Ctrl-p ',
"        \'Unite -no-split -auto-resize -buffer-name=MRU_File file_mru'],
"    \[' > 筛选当前缓冲区内容   Ctrl-l',
"        \'Unite -no-split -auto-resize -buffer-name=Lines line'],
"    \[' > 查看当前目录文件 ',
"        \'lcd %:p:h | Unite -no-split -auto-resize -start-insert -buffer-name=File_List file'],
"    \[' > 查看缓冲区 ',
"        \'Unite -no-split -quick-match -auto-resize -buffer-name=Buffer_List buffer'],
"    \[' > 增加文件书签        :UniteBookmarkAdd ',
"        \'UniteBookmarkAdd'],
"    \[' > 查看文件书签 ',
"        \'Unite -no-split -auto-resize -buffer-name=Bookmark bookmark'],
"    \[' > 查看复制寄存器内容 ',
"        \'Unite -no-split -quick-match -auto-resize -buffer-name=Yank_History history/yank'],
"    \]
"
"let g:unite_source_menu_menus.View = {
"    \ 'description' : ' > 视图设置切换操作（View）
"        \                            ',
"    \}
"
"let g:unite_source_menu_menus.View.command_candidates = [
"    \[' > 切换大纲视图（Markdown）   :VoomToggle markdown ',
"        \'VoomToggle markdown'],
"    \[' > Toggle Comment 切换注释    gcc ',
"        \'exe "normal! gcc"'],
"    \[' > 多行注释                   gcw ',
"        \'exe "normal! gcw"'],
"    \[' > 取消多行注释               gcuw ',
"        \'exe "normal! gcuw"'],
"    \[' > 切换颜色代码自动着色       :ColorToggle ',
"        \'ColorToggle'],
"    \[' > 设置为纯文本类型           :set filetype=txt ',
"        \'set filetype=txt'],
"    \[' > 切换当前窗口工作目录       <F1> ',
"        \'lcd %:p:h'],
"    \[' > 切换行号显示模式           <F2> ',
"        \'call ToggleRelativeNumber()'],
"    \[' > 切换搜索结果高亮显示       <F3> ',
"        \'call ToggleHLSearch()'],
"    \[' > 切换撤销历史显示           <F5> ',
"        \'UndotreeToggle'],
"    \[' > 切换自动换行               <F6> ',
"        \'call ToggleWrap()'],
"    \[' > 切换语法高亮显示           <F7> ',
"        \'call ToggleSyntaxHighlight()'],
"    \[' > 打开 TagList               <F8> ',
"        \'TagbarToggle'],
"    \[' > 切换缩进标志显示           :IndentLinesToggle',
"        \'IndentLinesToggle'],
"    \]
"let g:unite_source_menu_menus.Edit = {
"    \ 'description' : ' > 编辑操作（Edit）
"        \                            ',
"    \}
"
"
"let g:unite_source_menu_menus.Edit.command_candidates = [
"    \[' > 剔重排序，带 [!] 则反向排序     :sort[!] u ',
"        \'sort u'],
"    \[' > 排序并只保留最后一个            :g/^\(.*\)$\n\1$/d ',
"        \'g/^\(.*\)$\n\1$/d'],
"    \[' > Bookmark 增删书签               mm 或 :ToggleBookmark ',
"        \'ToggleBookmark'],
"    \[' > 编辑书签的描述说明              mi 或 :Annotate <TEXT> ',
"        \'echo ":Annotate <TEXT>" '],
"    \[' > 在新窗口中显示所有书签          ma 或 :ShowAllBookmarks ',
"        \'ShowAllBookmarks'],
"    \[' > 清除当前缓冲区书签              mc 或 :clearbookmarks ',
"        \'clearbookmarks'],
"    \[' > 删除 HTML 的标签部分            :%s#<[^>]\+>##g ',
"        \'%s#<[^>]\+>##g'],
"    \[' > Pathogen 更新插件帮助文档       :Helptags ',
"        \'Helptags'],
"    \[' > 转换为 GB2312 编码              :set fileencoding=cp936 ',
"        \'set fileencoding=cp936'],
"    \[' > 转换为 UTF-8 编码               :set fileencoding=utf-8 ',
"        \'set fileencoding=utf-8'],
"    \[' > 跳回最后编辑的位置              `. ',
"        \'exe "normal! `."'],
"    \[' > Case Switch 切换大小写           ~ ',
"        \'exe "normal! ~"'],
"    \[' > 显示光标位置，并统计字词        g<Ctrl-g> ',
"        \'exe "normal! g\<c-g>"'],
"    \[' > Align 对齐                      :Tabularize ',
"        \'echo ":Tabularize " '],
"    \[' > Mark 标记当前词                 <Leader>m ',
"        \'call mark#MarkCurrentWord()<CR>'],
"    \[' > Mark 标记指定模式               :Mark {pattern} ',
"        \'echo ":Mark {pattern}" '],
"    \[' > To Html 转换为网页文件          :TOhtml ',
"        \'TOhtml'],
"    \]
"
"let g:unite_source_menu_menus.Search = {
"    \ 'description' : ' > 搜索（Search）
"        \                            ',
"    \}
"
"let g:unite_source_menu_menus.Search.command_candidates = [
"    \[' > 遍历搜索文件内容     Ctrl-/ 或 :Ag [Options] Pattern [Path] ',
"        \'echo ":Ag [Options] Pattern [Path]" （--context 展示上下文）'],
"    \[' > 遍历搜索文件         :Ag [Options] -l Pattern [Path] ',
"        \'echo ":Ag [Options] -l Pattern [Path]" '],
"    \[' > Locate 列出搜索清单  :L[!] /<pattern1>/<pattern2>/.../[<flags>]） ',
"        \'echo ":L[!] /{pattern1}/{pattern2}/.../[{flags}]" '],
"    \]





"Move the preview window (code documentation) to the bottom of the screen, so it doesn't move the code!
"You might also want to look at the echodoc plugin
"set splitbelow

"Don't ask to save when changing buffers (i.e. when jumping to a type definition)
set hidden

" this setting controls how long to wait (in ms) before fetching type / symbol information.
"set updatetime=500
" Remove 'Press Enter to continue' message when type information is longer than one line.
set cmdheight=2


" omnicomplete settings
"don't autoselect first item in omnicomplete, show if only one item (for preview)
"remove preview if you don't want to see any documentation whatsoever.
set completeopt=longest,menuone,preview
" Fetch full documentation during omnicomplete requests.
" There is a performance penalty with this (especially on Mono)
" By default, only Type/Method signatures are fetched. Full documentation can still be fetched when
" you need it with the :OmniSharpDocumentation command.
" let g:omnicomplete_fetch_documentation=1

" Showmatch significantly slows down omnicomplete
" when the first match contains parentheses.
"set noshowmatch

" Syntastic settings
" Get Code Issues and syntax errors
"
let g:syntastic_cs_checkers = ['syntax', 'semantic', 'issues']



" OmniSharp won't work without this setting
filetype plugin on

"This is the default value, setting it isn't actually necessary
let g:OmniSharp_host = "http://localhost:2000"

"Set the type lookup function to use the preview window instead of the status line
let g:OmniSharp_typeLookupInPreview = 1

"Timeout in seconds to wait for a response from the server
let g:OmniSharp_timeout = 1

let g:OmniSharp_selector_ui = 'unite'  " Use unite.vim
"let g:OmniSharp_selector_ui = 'ctrlp'  " Use ctrlp.vim

"如果安装了 vim-dispatch ，是否自动开启 关闭server
"let g:Omnisharp_start_server = 1
"let g:Omnisharp_stop_server = 0

" sever类型
let g:OmniSharp_server_type = 'v1'
"let g:OmniSharp_server_type = 'roslyn'


" Get Code Issues and syntax errors
let g:syntastic_cs_checkers = ['syntax', 'semantic', 'issues']
" If you are using the omnisharp-roslyn backend, use the following
" let g:syntastic_cs_checkers = ['code_checker']
augroup omnisharp_commands
    autocmd!

    "Set autocomplete function to OmniSharp (if not using YouCompleteMe completion plugin)
    autocmd FileType cs setlocal omnifunc=OmniSharp#Complete

    " Synchronous build (blocks Vim)
    "autocmd FileType cs nnoremap <F5> :wa!<cr>:OmniSharpBuild<cr>
    " Builds can also run asynchronously with vim-dispatch installed
    autocmd FileType cs nnoremap <leader>b :wa!<cr>:OmniSharpBuildAsync<cr>
    " automatic syntax check on events (TextChanged requires Vim 7.4)
    autocmd BufEnter,TextChanged,InsertLeave *.cs SyntasticCheck

    " Automatically add new cs files to the nearest project on save
    autocmd BufWritePost *.cs call OmniSharp#AddToProject()

    "show type information automatically when the cursor stops moving
    autocmd CursorHold *.cs call OmniSharp#TypeLookupWithoutDocumentation()

    "The following commands are contextual, based on the current cursor position.

    autocmd FileType cs nnoremap gd :OmniSharpGotoDefinition<cr>
    autocmd FileType cs nnoremap <leader>fi :OmniSharpFindImplementations<cr>
    autocmd FileType cs nnoremap <leader>ft :OmniSharpFindType<cr>
    autocmd FileType cs nnoremap <leader>fs :OmniSharpFindSymbol<cr>
    autocmd FileType cs nnoremap <leader>fu :OmniSharpFindUsages<cr>
    "finds members in the current buffer
    autocmd FileType cs nnoremap <leader>fm :OmniSharpFindMembers<cr>
    " cursor can be anywhere on the line containing an issue
    autocmd FileType cs nnoremap <leader>x  :OmniSharpFixIssue<cr>
    autocmd FileType cs nnoremap <leader>fx :OmniSharpFixUsings<cr>
    autocmd FileType cs nnoremap <leader>tt :OmniSharpTypeLookup<cr>
    autocmd FileType cs nnoremap <leader>dc :OmniSharpDocumentation<cr>
    "navigate up by method/property/field
    autocmd FileType cs nnoremap <C-K> :OmniSharpNavigateUp<cr>
    "navigate down by method/property/field
    autocmd FileType cs nnoremap <C-J> :OmniSharpNavigateDown<cr>

augroup END


" Contextual code actions (requires CtrlP or unite.vim)
nnoremap <leader><space> :OmniSharpGetCodeActions<cr>
" Run code actions with text selected in visual mode to extract method
vnoremap <leader><space> :call OmniSharp#GetCodeActions('visual')<cr>

" rename with dialog
nnoremap <leader>nm :OmniSharpRename<cr>
nnoremap <F2> :OmniSharpRename<cr>
" rename without dialog - with cursor on the symbol to rename... ':Rename newname'
command! -nargs=1 Rename :call OmniSharp#RenameTo("<args>")

" Force OmniSharp to reload the solution. Useful when switching branches etc.
nnoremap <leader>rl :OmniSharpReloadSolution<cr>
nnoremap <leader>cf :OmniSharpCodeFormat<cr>
" Load the current .cs file to the nearest project
nnoremap <leader>tp :OmniSharpAddToProject<cr>

" (Experimental - uses vim-dispatch or vimproc plugin) - Start the omnisharp server for the current solution
nnoremap <leader>ss :OmniSharpStartServer<cr>
nnoremap <leader>sp :OmniSharpStopServer<cr>

" Add syntax highlighting for types and interfaces
nnoremap <leader>th :OmniSharpHighlightTypes<cr>


